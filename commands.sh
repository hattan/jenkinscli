#!/usr/bin/env bash

start() {
  sudo systemctl start jenkins.service
  _success "Jenkins service started."
}

stop() {
  sudo systemctl stop jenkins.service
  _success "Jenkins service stopped."
}

restart() {
  sudo systemctl restart jenkins.service
  _success "Jenkins service restaret."
}

status() {
  sudo systemctl status jenkins.service
}

logs() {
  sudo journalctl -fu jenkins.service
}

install() {
  # systemd service
  sudo cp $SCRIPT_PATH/jenkins.service /etc/systemd/system/jenkins.service
  sudo sudo systemctl enable jenkins.service

  start
  
  # Sym Link	
  local path=/usr/local/bin/jenkins
  sudo ln -s $SCRIPT_PATH/jenkins.sh $path
  _success "Jenkins symlink installed at $path"
}



