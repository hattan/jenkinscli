``` 
    ___  _______   ________   ___  __    ___  ________   ________
   |\  \|\  ___ \ |\   ___  \|\  \|\  \ |\  \|\   ___  \|\   ____\
   \ \  \ \   __/|\ \  \\ \  \ \  \/  /|\ \  \ \  \\ \  \ \  \___|_
 __ \ \  \ \  \_|/_\ \  \\ \  \ \   ___  \ \  \ \  \\ \  \ \_____  \
|\  \\_\  \ \  \_|\ \ \  \\ \  \ \  \\ \  \ \  \ \  \\ \  \|____|\  \
\ \________\ \_______\ \__\\ \__\ \__\\ \__\ \__\ \__\\ \__\____\_\  \
 \|________|\|_______|\|__| \|__|\|__| \|__|\|__|\|__| \|__|\_________\
                                                           \|_________|

 version: 0.0.1

 Custom tool to make it easy to invoke the jenkins cli jar and to work with Jenkins service


Usage: jenkins <command>

 This tool invokes custom script commands and can also be used for running the jenkins cli jar.
 If the arguments passed to the tool  do not match a custom command, the entire remaining string is sent to the jenkins cli jar.

  custom script commands:
     start         : Start the Jenkins service.
     stop          : Stop the Jenkins service.
     restart       : Restart the Jenkins service.
     status        : Display the current status of the Jenkins service.
     logs          : Show the Jenkins server logs.
     install       : Install the Jenkins Service and Create the jenkins tool symlink.
     -h | --help   : Show the help document (this.)

   jenkins cli commands:
     *             : Input parameters that do not match the above commands are passed directly to the jenkins-cli.jar.
     eg. jenkins list-jobs will invoke the jenkins cli with the list-jobs command
```

# Configuration

```shell
mkdir -p ~/.custom-tools/scripts
cd ~/.custom-tools/scripts
git clone git@gitlab.com:hattan/jenkinscli.git
touch cli.env
# Open cli.env in VM and add the following two variables ()
# JENKINS_URL=<URL to your jenkins instance>
# AUTH_TOKEN=<Jenkins Auth token>
./jenkins.sh install
```

## Development
The main entrypoint for this tool is [jenkins.sh](./jenkins.sh). All other files are libraries and sourced inside jenkins.sh

Note: Some of the paths are hardcoded to the /home/jenkinsadmin. These need to refactored to be more generic.
