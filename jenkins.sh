#!/usr/bin/env bash

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
source $SCRIPT_PATH/fmt.sh
source $SCRIPT_PATH/commands.sh
source $SCRIPT_PATH/usage.sh
source $SCRIPT_PATH/cli.sh

declare DEBUG=false
declare FLAGS=""

# handle no commands
if [ "$#" -eq 0 ]; then
  usage
  exit 1
fi

# parse inputs
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -h | --help)
        usage
        exit 0
        ;;
    start)
        start
	exit 0
        ;;
    stop)
        stop
	exit 0
        ;;
    restart)
        restart
	exit 0
        ;;
    status)
       status
       exit 0
       ;;
    logs)
       logs
       exit 0
       ;;
    install)
       install
       exit 0
       ;;
    *)  
        FLAGS+="${1} "
       ;;
  esac
  shift
done
FLAGS=$(echo $FLAGS | sed -e 's/^[ \t]*//')

# not a command this tool can handle, pass arguments to jenkins cli
invoke_cli "$FLAGS"
