
print_banner(){
  cat << "EOF"

    ___  _______   ________   ___  __    ___  ________   ________      
   |\  \|\  ___ \ |\   ___  \|\  \|\  \ |\  \|\   ___  \|\   ____\     
   \ \  \ \   __/|\ \  \\ \  \ \  \/  /|\ \  \ \  \\ \  \ \  \___|_    
 __ \ \  \ \  \_|/_\ \  \\ \  \ \   ___  \ \  \ \  \\ \  \ \_____  \   
|\  \\_\  \ \  \_|\ \ \  \\ \  \ \  \\ \  \ \  \ \  \\ \  \|____|\  \  
\ \________\ \_______\ \__\\ \__\ \__\\ \__\ \__\ \__\\ \__\____\_\  \ 
 \|________|\|_______|\|__| \|__|\|__| \|__|\|__|\|__| \|__|\_________\
                                                           \|_________|

EOF

  echo " version: $(cat $SCRIPT_PATH/version.txt)"
  echo ""                                                         
  echo " Custom tool to make it easy to invoke the jenkins cli jar and to work with Jenkins service"                                                         
  echo ""   
}

usage() {
  print_banner
  _helpText=" Usage: jenkins <command>

 This tool invokes custom script commands and can also be used for running the jenkins cli jar. 
 If the arguments passed to the tool  do not match a custom command, the entire remaining string is sent to the jenkins cli jar.

  custom script commands:
     start         : Start the Jenkins service.
     stop          : Stop the Jenkins service.
     restart       : Restart the Jenkins service.
     status        : Display the current status of the Jenkins service.
     logs          : Show the Jenkins server logs.
     install       : Install the Jenkins Service and Create the jenkins tool symlink.
     -h | --help   : Show the help document (this.)

   jenkins cli commands:
     * 	           : Input parameters that do not match the above commands are passed directly to the jenkins-cli.jar.
     eg. jenkins list-jobs will invoke the jenkins cli with the list-jobs command
"      
        _information "$_helpText" 1>&2
        exit 0  
}
