#!/usr/bin/env bash

read_env_file(){
  set -o allexport && . $SCRIPT_PATH/cli.env && set +o allexport
  
}

invoke_cli() {
  # Load ENV Vars
  read_env_file

  # Run jenkins-cli
  local flags=$1
  java -jar /home/jenkinsadmin/jenkins-cli.jar -s $JENKINS_URL -auth $AUTH_TOKEN $FLAGS

  # Cleanup
  unset JENKINS_URL
  unset AUTH_TOKEN
}


